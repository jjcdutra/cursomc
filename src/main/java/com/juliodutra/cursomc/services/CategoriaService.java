package com.juliodutra.cursomc.services;

import com.juliodutra.cursomc.domains.Categoria;
import com.juliodutra.cursomc.repositories.CategoriaRepository;
import com.juliodutra.cursomc.services.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoriaService {

    @Autowired private CategoriaRepository repo;

    public Categoria buscar(Integer id) {
        Optional<Categoria> obj = repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado, id: " + id + ", tipo: " + Categoria.class.getName()));
    }
}
