package com.juliodutra.cursomc.services;

import com.juliodutra.cursomc.domains.Cliente;
import com.juliodutra.cursomc.repositories.ClienteRepository;
import com.juliodutra.cursomc.services.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired private ClienteRepository repo;

    public Cliente buscar(Integer id) {
        Optional<Cliente> obj = repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado, id: " + id + ", tipo: " + Cliente.class.getName()));
    }
}
